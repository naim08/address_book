<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\File;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {

        $contacts = $this->getDoctrine()
                            ->getRepository('AppBundle:Contact')
                            ->findAll();
        
        return $this->render('default/index.html.twig', [
            'contacts' => $contacts
        ]);
    }

    /**
     * @Route("/create", name="contact_create")
     */
    public function createAction(Request $request)
    {
        $contact = new \AppBundle\Entity\Contact();
        
        $form = $this->getContactForm($contact);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            $contact = $this->updateContact($form, $contact);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            
            $this->addFlash('notice', 'New Contact Added');
            
            return $this->redirectToRoute('homepage');
        }

        return $this->render('default/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="contact_edit")
     */
    public function editAction($id, Request $request)
    {
        $contact = $this->getDoctrine()
                ->getRepository('AppBundle:Contact')
                ->find($id);
        
        if (empty($contact)) {
            $this->addFlash('error', 'Contact not found');
            
            return $this->redirectToRoute('homepage');
        }
        
        $form = $this->getContactForm($contact, true);
        $form->handleRequest($request);
        
        if($form->isSubmitted() && $form->isValid()) {
            $contact = $this->updateContact($form, $contact);
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            
            $this->addFlash('notice', 'Contact Updated');
            
            return $this->redirectToRoute('homepage');
        }
        
        return $this->render('default/edit.html.twig', array(
            'form' => $form->createView(),
            'contact' => $contact
        ));
    }

    /**
     * @Route("/details/{id}", name="contact_details")
     */
    public function detailsAction($id)
    {
        $contact = $this->getDoctrine()
                ->getRepository('AppBundle:Contact')
                ->find($id);
        if (empty($contact)) {
            $this->addFlash('error', 'Contact not found');
            
            return $this->redirectToRoute('homepage');
        }
        
        return $this->render('default/detail.html.twig', [
            'contact' => $contact
        ]);
    }

    /**
     * @Route("/delete/{id}", name="contact_delete")
     */
    public function deleteAction($id)
    {
        $contact = $this->getDoctrine()
                ->getRepository('AppBundle:Contact')
                ->find($id);
        
        if (empty($contact)) {
            $this->addFlash('error', 'Contact not found');
            
            return $this->redirectToRoute('homepage');
        }
        
        $em = $this->getDoctrine()->getManager();
        $em->remove($contact);
        $em->flush();
        
        $this->addFlash('notice', 'Contact removed');
       
        return $this->redirectToRoute('homepage');
    }

    /**
     * @return object
     */
    private function getContactForm($contact, $isUpdate=false)
    {
        $pictureName = '';
        $submitButtonText = 'Create Contact';
        if ($isUpdate) {
            $submitButtonText = 'Update Contact';

            if (!empty($contact->getPicture())) {
                $pictureName = $contact->getPicture();

                $contact->setPicture(
                    new File($this->getParameter('picture_directory').'/'.$contact->getPicture())
                );
            }
        }

        $atrributes = ['class' => 'form-control mb-3'];
        $form = $this->createFormBuilder($contact)
                ->add('firstname', TextType::class)
                ->add('lastname', TextType::class)
                ->add('streetAndNumber', TextType::class)
                ->add('zip', NumberType::class)
                ->add('city', TextType::class)
                ->add('country', TextType::class)
                ->add('phonenumber', TextType::class)
                ->add('birthday', DateType::class, [
                    'widget' => 'choice', 
                    'years' => range(date('Y') - 100, date('Y'))
                ])
                ->add('email', EmailType::class)
                ->add('picture', FileType::class, [
                    'required' => false,
                    'attr' => ['accept' => 'image/jpeg,image/png,image/jpg,image/svg']
                ])
                ->add('pictureName', HiddenType::class, [
                    'data' => $pictureName,
                    'mapped' => false
                ])
                ->add('save', SubmitType::class, [
                    'label' => $submitButtonText, 
                    'attr' => ['class' => 'btn btn-primary mb-5']
                ])
                ->getForm();

        return $form;
    } 

    /**
     * @return object
     */
    private function updateContact($form, $contact)
    {
        // Set properties
        $contact->setFirstname($form['firstname']->getData());
        $contact->setLastname($form['lastname']->getData());
        $contact->setStreetAndNumber($form['streetAndNumber']->getData());
        $contact->setZip($form['zip']->getData());
        $contact->setCity($form['city']->getData());
        $contact->setCountry($form['country']->getData());
        $contact->setPhonenumber($form['phonenumber']->getData());
        $contact->setBirthday($form['birthday']->getData());
        $contact->setEmail($form['email']->getData());

        // Set picture
        $file = $contact->getPicture();
        if ($file) {
            $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

            try {
                $file->move(
                    $this->getParameter('picture_directory'),
                    $fileName
                );
            } catch (FileException $e) {
                $this->addFlash('notice', 'Image was not successfully uploaded');
            }

            $contact->setPicture($fileName);
        } elseif ($form['pictureName']->getData()) {
            $contact->setPicture($form['pictureName']->getData());
        }

        return $contact;
    }

     /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

}
